(defpackage blue-book/tests/main
  (:use :cl
        :blue-book)
  (:local-nicknames (:rv :rove)
(in-package :blue-book/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :blue-book)' in your Lisp.

(rv:deftest test-target-1
  (rv:testing "should (= 1 1) to be true"
    (rv:ok (= 1 1))))
