(defsystem "blue-book"
  :version "0.1.0"
  :author "Sam Johnson"
  :license "AGPLv3 or Later"
  :depends-on ("alexandria"
               "serapeum"
               "cl-arrows")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "Lisp-Flutter Compatibility Layer; write Lisp Once, run on 7 platforms!"
  :in-order-to ((test-op (test-op "blue-book/tests")))
  ;; Build a binary:
  ;; don't change this line.
  :build-operation "program-op"
  ;; binary name: adapt.
  :build-pathname "blue-book"
  ;; entry point: here "main" is an exported symbol. Otherwise, use a double ::
  :entry-point "blue-book:main")


(defsystem "blue-book/tests"
  :author "Sam Johnson"
  :license "AGPLv3 or Later"
  :depends-on ("blue-book"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for blue-book"
  :perform (test-op (op c) (symbol-call :rove :run c)))
